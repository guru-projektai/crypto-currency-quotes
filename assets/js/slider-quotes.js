jQuery(function ($) {

  $('.crypto-icon').tooltip({
    container: 'body',
    template: '<div class="tooltip crypto-title-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  });

  $('.crypto-quotes-slides').each(function (index) {
    // $(this).on('init', function() {
    //   $('.left-btn').addClass('slick-disabled');
    // });

    let checkPhpParams = () => {
      if (typeof sliderAttribute === 'undefined') return true;

      return sliderAttribute.isPost === 'true';
    }

    let isPost = checkPhpParams();

    $(this).slick({
      infinite: false,
      slidesToShow: isPost ? 3 : 3,
      slidesToScroll: 3,
      arrows: true,
      nextArrow:
        '<button type="button" class="slick-button right-btn">' +
        '<span class="slick-button-wrap">' +
        '<svg xmlns="http://www.w3.org/2000/svg" height="100%" version="1.1" viewBox="0 0 512 512" width="100%">' +
        '<path d="M342.15 256L126.78 40.67A20.32 20.32 0 1 1 155.53 12l229.66 229.63a20.3 20.3 0 0 1 0 28.71L155.53 500a20.32 20.32 0 1 1-28.75-28.71z" fill="#fff"></path>' +
        '</svg>' +
        '</span>' +
        '</button>',
      prevArrow:
        '<button type="button" class="slick-button left-btn">' +
        '<span class="slick-button-wrap">' +
        '<svg xmlns="http://www.w3.org/2000/svg" height="100%" version="1.1" viewBox="0 0 512 512" width="100%">' +
        '<path d="M169.85 256l215.37 215.33A20.32 20.32 0 1 1 356.47 500L126.81 270.37a20.3 20.3 0 0 1 0-28.71L356.47 12a20.32 20.32 0 1 1 28.75 28.71z" fill="#fff"></path>' +
        '</svg>' +
        '</span>' +
        '</button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: true
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
            autoplay: false,
            autoplaySpeed: 5000,
            arrows: true,
            variableWidth: true,
            infinite: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: true,
            variableWidth: true,
            infinite: true,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: true,
            variableWidth: false,
            infinite: true,
          }
        }
      ]
    });

    // let currentSlide = $(this).slick('slickCurrentSlide');
    //
    // let leftBtn = $('.left-btn');
    //
    // if (currentSlide !== 0) {
    //   $(leftBtn).removeClass('slick-disabled');
    // }
    //
    // $(this).one('afterChange', function() {
    //   $(leftBtn).removeClass('slick-disabled');
    //   $(leftBtn).css({
    //     'opacity': 1,
    //     'pointer-events': 'auto'
    //   });
    // });
  })
})
