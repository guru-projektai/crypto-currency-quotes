<?php
/**
 * Plugin Name: Crypto currency quotes
 * Description: Crypto currency quotes from coinmarketcap
 * Version: 2.1.0
 * Author: Mindaugas && Alex R.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die;
}

define( 'Crypto_Currency_Quotes_FILE', __FILE__ );
define( 'Crypto_Currency_Quotes_PATH', plugin_dir_path( Crypto_Currency_Quotes_FILE ) );
define( 'Crypto_Currency_Quotes_URL', plugin_dir_url( Crypto_Currency_Quotes_FILE ) );
define( 'CCQ_CRYPTO_API', "https://pro-api.coinmarketcap.com/v1/cryptocurrency/" );
define( 'CCQ_API_KEY', "587553e0-f6f7-4635-93b1-cfc28e8f226f" );
define( 'CCQ_CURRENCY_CONVERT', "USD" );
define( 'CCQ_PODS_DATA_TABLE', "cryptocurrency" );
define( 'CCQ_SCHEDULE_HOOK_NAME', "ccq_update_data" );

// register_deactivation_hook( Crypto_Currency_Quotes_FILE, array( 'Crypto_Currency_Quotes', 'ccq_deactivate' ) );

final class Crypto_Currency_Quotes {

	// Must update this array in the future when
	// adding new columns together with json migration
	const PLUGIN_DB_COLUMNS = [
		'name',
		'short_name',
		'slug',
		'rank',
		'price',
		'percent_change_1h',
		'percent_change_24h',
		'percent_change_7d',
		'last_updated',
		'ratio',
        'status'
	];

	/**
	 * Plugin instance.
	 *
	 * @var Crypto_Currency_Quotes
	 * @access private
	 */
	private static $instance = null;

	/**
	 * Get plugin instance.
	 *
	 * @return Crypto_Currency_Quotes
	 * @static
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Constructor.
	 *
	 * @access private
	 */
	private function __construct() {
		register_activation_hook( Crypto_Currency_Quotes_FILE, array( $this, 'ccq_activate' ) );

		$this->ccq_includes();

		new Crypto_Currency_Quotes_Admin();

		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array(
			$this,
			'ccq_add_plugin_page_settings_link'
		) );

        add_action( 'wp_ajax_update_status',  array( $this, 'ccq_update_item_status'));
		add_action( 'activated_plugin', array( $this, 'save_error' ) );

		//add_action( 'wp_enqueue_scripts', array($this, 'enqueue'));

		// Create shortcode to use it anywhere
		add_shortcode( 'crypto-quotes', array( $this, 'ccq_shortcode' ) );
        add_shortcode( 'crypto-quotes-new', array( $this, 'ccq_shortcode_new' ) );

		// Create action in order to use it later for schedule events
		add_action( 'ccq_update_data', array( $this, 'ccq_data_autoupdater' ) );

		// Adds new interval for cron events
		//add_filter( 'cron_schedules', array( $this, 'add_cron_recurrence_interval' ) );

		add_action( 'admin_init', array( $this, 'migrate_columns' ) );
		add_action( 'admin_menu', 'cryptocurrencies_settings_menu');

	}

	/**
	 * Enqueue styles and js
	 */
	public function enqueue() {
		wp_enqueue_style( 'ccq-styles', plugins_url( '/assets/dist/styles.css', __FILE__ ) );
		wp_enqueue_style( 'slick-styles', plugins_url( '/assets/css/slick.css', __FILE__ ) );

		wp_register_script( 'slider-js', plugins_url( '/assets/js/slider-quotes.js', __FILE__ ), array( 'jquery' ) );

		wp_localize_script( 'slider-js', 'sliderAttribute', array(
			'isPost' => is_single() ? 'true' : 'false',
		) );

		wp_enqueue_script( 'slider-js' );
	}

	/**
	 * Saves error to txt file (in case)
	 */
	public function save_error() {
		file_put_contents( dirname( __file__ ) . '/error_activation.txt', ob_get_contents() );
	}

	/**
	 * Add settings page
	 *
	 * @param $links
	 *
	 * @return array
	 */
	public function ccq_add_plugin_page_settings_link( $links ) {
		$links[] = '<a href="' .
		           admin_url( 'options-general.php?page=' . Crypto_Currency_Quotes_Admin::MENU_URL ) .
		           '">' . __( 'Settings' ) . '</a>';

		return $links;
	}

	/**
	 * Run when deactivate plugin.
	 *
	 * Deletes pods database
	 * Deletes plugin options
	 * Deletes scheduled hooks
	 */
	public static function ccq_deactivate() {

		wp_unschedule_hook( CCQ_SCHEDULE_HOOK_NAME );
	}

	/**
	 * Run when activate plugin.
	 *
	 * Runs migration (database)
	 * Imports default settings (API URL, API key, etc...)
	 * Fetches from API first time to have data
	 */

	public function ccq_activate() {
		$this->migrate();
		$this->import_default_settings();
		$this->ccq_data_autoupdater();
	}

    public function ccq_update_item_status() {
        global $wpdb;
        $item_id = intval($_POST['item_id']);
        $status = intval($_POST['status']);

        $table_name = $wpdb->prefix . 'cryptocurrency';
        $wpdb->update($table_name, array('status' => $status), array('ID' => $item_id));

        echo 'Status updated';
        wp_die();
    }

	/**
	 * Plugin shortcode
	 *
	 * @param array $atts
	 *
	 * @return false|string
	 */
	public function ccq_shortcode( $atts = [] ) {
		$data     = get_data();
		$currency = get_currency_symbol( get_option( Crypto_Currency_Quotes_Admin::OPTIONS )[ Crypto_Currency_Quotes_Admin::CURRENCY_CONVERT_FIELD_NAME ] );

		ob_start();

		include Crypto_Currency_Quotes_PATH . 'templates/crypto-quotes-shortcode.php';

		return ob_get_clean();
	}

    /**
     * Plugin shortcode from API coinstats.app
     *
     * @param array $atts
     *
     * @return false|string
     */
    public function ccq_shortcode_new( $atts = [] ) {
        $locale = get_locale();
        $_language = explode('_', $locale);
        $language = $_language[0];
        $json_coin = $this->get_json_coins();
        $api_url = get_option( Crypto_Currency_Quotes_Admin::OPTIONS )[ Crypto_Currency_Quotes_Admin::NEW_API_URL_FIELD_NAME ];
        $api_key = get_option( Crypto_Currency_Quotes_Admin::OPTIONS )[ Crypto_Currency_Quotes_Admin::NEW_API_KEY_FIELD_NAME ];
        ob_start();

        include Crypto_Currency_Quotes_PATH . 'templates/crypto-quotes-shortcode-new.php';

        return ob_get_clean();
    }

	public static function ccq_getData( $atts = [] ) {
		$data     = get_data();
		$currency = get_currency_symbol( get_option( Crypto_Currency_Quotes_Admin::OPTIONS )[ Crypto_Currency_Quotes_Admin::CURRENCY_CONVERT_FIELD_NAME ] );
		$cryptos  = [];
		foreach ( $data as $key => $crypto ) {
			$cryptos[ $key ]['name']       = $crypto->name;
			$cryptos[ $key ]['short_name'] = $crypto->short_name;
			$cryptos[ $key ]['logo_url']   = plugin_dir_url( __FILE__ ) . 'assets/images/cryptos/' . strtolower( $crypto->short_name ) . '.svg';
			$cryptos[ $key ]['price']      = $currency . ' ' . round( $crypto->price, 2 );
			$cryptos[ $key ]['change']     = !empty($crypto->percent_change_24h) ? $crypto->percent_change_24h : '';
			$cryptos[ $key ]['link']       = $crypto->link;
		}

		return $cryptos;
	}

    public function  get_json_coins() {
        global $wpdb;
        $sql = "SELECT ref_link, short_name FROM {$wpdb->prefix}cryptocurrency WHERE status = 1 order by `rank`";
        $result = $wpdb->get_results($sql, ARRAY_A);
        if (!empty($result)) {
            $coins = array();
            $i = 0;
            foreach ($result as $row) {
                if ($row['ref_link'] == null) continue;
                $coins[$i]['symbol'] = $row['short_name'];
                $coins[$i]['link'] = get_permalink($row['ref_link']);
                $i++;
            }
            $json_coin = json_encode($coins);
            return $json_coin;
        }
        return null;
    }

	/**
	 * @param $schedules
	 *
	 * @return mixed
	 */
	public function add_cron_recurrence_interval( $schedules ) {
		$schedules['every_10_mins'] = array(
			'interval' => 600,
			'display'  => __( 'Every 10 minutes', 'textdomain' )
		);

		return $schedules;
	}

	/**
	 * Loading plugin functions files
	 */
	public function ccq_includes() {
		require_once __DIR__ . '/includes/ccq-functions.php';
		require_once __DIR__ . '/includes/admin-page.php';
		require_once __DIR__ . '/includes/ccq-widget.php';
	}

	/**
	 * Update database after specific interval
	 */
	public function ccq_data_autoupdater() {
		if ( false === as_next_scheduled_action( 'hook_ccq_get_coinmarketcap_api_data' ) ) {
			// Repeat daily to have fresh data.
			as_schedule_recurring_action(
				time(),
				1 * 60 * 60, //run update every 1 hour
				'hook_ccq_get_coinmarketcap_api_data',
				[],
				"crypto_currency"
			);
		}
		add_action( 'hook_ccq_get_coinmarketcap_api_data', 'ccq_get_coinmarketcap_api_data', 10 );
	}

	/**
	 * Migrate necessary database through pods
	 */
	private function migrate() {
		global $wpdb;
		$table_name   = $wpdb->prefix . CCQ_PODS_DATA_TABLE; // Replace 'your_table_name' with the actual name of the table you want to check
		$table_exists = $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) === $table_name;
		if ( ! $table_exists ) {
			//$dump = file_get_contents( Crypto_Currency_Quotes_PATH . 'imports/wp_pods_cryptocurrency.sql' );
			// Execute SQL statements
			global $wpdb;
			// set the default character set and collation for the table
			$charset_collate = $wpdb->get_charset_collate();
			// Check that the table does not already exist before continuing
			$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->base_prefix}".CCQ_PODS_DATA_TABLE."` (
					  `id` bigint(20) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `rank` decimal(12,0) DEFAULT NULL,
					  `price` decimal(64,6) DEFAULT NULL,
					  `percent_change_1h` decimal(12,2) DEFAULT NULL,
					  `percent_change_24h` decimal(12,2) DEFAULT NULL,
					  `percent_change_7d` decimal(12,2) DEFAULT NULL,
					  `last_updated` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `ratio` decimal(64,10) DEFAULT NULL,
					  `ref_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `status` tinyint COLLATE utf8mb4_unicode_ci DEFAULT 0,
					  PRIMARY KEY (id)
					  ) $charset_collate;";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
			$is_error = empty( $wpdb->last_error );
			return $is_error;
		}

	}

	/**
	 * Migrate columns
	 */
	public function migrate_columns() {
		$this->ccq_data_autoupdater();
	}

	/**
	 * Imports default settings
	 */
	private function import_default_settings() {
		if ( ! get_option( Crypto_Currency_Quotes_Admin::OPTIONS ) ) {
			add_option( Crypto_Currency_Quotes_Admin::OPTIONS, [
				Crypto_Currency_Quotes_Admin::API_KEY_FIELD_NAME          => CCQ_API_KEY,
				Crypto_Currency_Quotes_Admin::API_URL_FIELD_NAME          => CCQ_CRYPTO_API,
				Crypto_Currency_Quotes_Admin::CURRENCY_CONVERT_FIELD_NAME => CCQ_CURRENCY_CONVERT,
			] );
		}
	}
}

function Crypto_Currency_Quotes() {
	return Crypto_Currency_Quotes::get_instance();
}

$GLOBALS['Crypto_Currency_Quotes'] = Crypto_Currency_Quotes();