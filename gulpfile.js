const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const groupmq = require('gulp-group-css-media-queries');
const bs = require('browser-sync');
const csso = require('gulp-csso');

const SASS_SOURCES = [
  './assets/scss/*.scss',
];

/**
 * Compile Sass files
 */
gulp.task('compile:sass', () =>
  gulp.src(SASS_SOURCES)
    .pipe(plumber())
    .pipe(sass({
      indentType: 'tab',
      indentWidth: 1,
      outputStyle: 'expanded',
    })).on('error', sass.logError)
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(groupmq()) // Group media queries!
    .pipe(csso())
    .pipe(gulp.dest('./assets/dist/'))
    .pipe(bs.stream())); // Stream to browserSync


/**
 * Start up browserSync and watch Sass files for changes
 */
gulp.task('watch:sass', ['compile:sass'], () => {
  bs.init({
    proxy: 'https://smartbettingguide.test'
  });

  gulp.watch(SASS_SOURCES, ['compile:sass']);
});

/**
 * Default task executed by running `gulp`
 */
gulp.task('default', ['watch:sass']);