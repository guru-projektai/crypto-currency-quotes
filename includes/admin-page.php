<?php

class Crypto_Currency_Quotes_Admin
{
    const API_KEY_FIELD_NAME = 'coinmarket_api_key';
    const API_URL_FIELD_NAME = 'coinmarket_api_url';
    const CURRENCY_CONVERT_FIELD_NAME = 'currency_convert';
    const NEW_API_KEY_FIELD_NAME = 'coinstats_api_key';
    const NEW_API_URL_FIELD_NAME = 'coinstats_api_url';

    const OPTIONS = 'crypto_quotes_options';
    const MENU_URL = 'crypto_quotes_settings';

    /** @var array */
    private $options;

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        add_options_page(
            'Crypto Quotes settings',
            'Crypto Quotes settings',
            'manage_options',
            self:: MENU_URL,
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        $this->options = get_option(self::OPTIONS);
        ?>
        <div class="wrap">
            <h1>Crypto Quotes settings</h1>
            <form method="post" action="options.php">
                <?php
                settings_fields( 'crypto_quotes_group' );
                do_settings_sections( 'crypto-quotes-settings' );
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'crypto_quotes_group', // Option group
            self::OPTIONS, // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Crypto quotes settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'crypto-quotes-settings' // Page
        );

        add_settings_field(
            'coinmarket_api_url', // ID
            'Coinmarket API URL', // Title
            array( $this, 'api_url_callback' ), // Callback
            'crypto-quotes-settings', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'coinmarket_api_key',
            'Coinmarket API Key',
            array( $this, 'api_key_callback' ),
            'crypto-quotes-settings',
            'setting_section_id'
        );

        add_settings_field(
            'currency_convert',
            'Currency Convert (Crypto - Currency exchange)',
            array( $this, 'currency_convert_callback' ),
            'crypto-quotes-settings',
            'setting_section_id'
        );

        add_settings_field(
            'coinstats_api_url',
            'coinstats.app API URL',
            array( $this, 'currency_convert_callback_api_url' ),
            'crypto-quotes-settings',
            'setting_section_id'
        );

        add_settings_field(
            'coinstats_api_key',
            'coinstats.app API KEY',
            array( $this, 'currency_convert_callback_api_key' ),
            'crypto-quotes-settings',
            'setting_section_id'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input
     * @return array
     */
    public function sanitize($input)
    {
        $new_input = array();

        if( isset($input['coinmarket_api_url']) )
            $new_input['coinmarket_api_url'] = sanitize_text_field( $input['coinmarket_api_url']);

        if( isset($input['coinmarket_api_key']) )
            $new_input['coinmarket_api_key'] = sanitize_text_field($input['coinmarket_api_key']);

        if( isset($input['currency_convert']) )
            $new_input['currency_convert'] = sanitize_text_field($input['currency_convert']);

        if( isset($input['coinstats_api_url']) )
            $new_input['coinstats_api_url'] = sanitize_text_field($input['coinstats_api_url']);

        if( isset($input['coinstats_api_key']) )
            $new_input['coinstats_api_key'] = sanitize_text_field($input['coinstats_api_key']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your crypto quotes settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_url_callback()
    {
        printf(
            '<input type="text" id="coinmarket_api_url" name="crypto_quotes_options[coinmarket_api_url]" value="%s" />',
            isset( $this->options['coinmarket_api_url'] ) ? esc_attr( $this->options['coinmarket_api_url']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_key_callback()
    {
        printf(
            '<input type="text" id="coinmarket_api_key" name="crypto_quotes_options[coinmarket_api_key]" value="%s" />',
            isset( $this->options['coinmarket_api_key'] ) ? esc_attr( $this->options['coinmarket_api_key']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function currency_convert_callback()
    {
        printf(
            '<input type="text" id="currency_convert" name="crypto_quotes_options[currency_convert]" value="%s" />',
            isset( $this->options['currency_convert'] ) ? esc_attr( $this->options['currency_convert']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function currency_convert_callback_api_url()
    {
        printf(
            '<input type="text" id="coinstats_api_url" name="crypto_quotes_options[coinstats_api_url]" value="%s" />',
            isset( $this->options['coinstats_api_url'] ) ? esc_attr( $this->options['coinstats_api_url']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function currency_convert_callback_api_key()
    {
        printf(
            '<input type="text" id="coinstats_api_key" name="crypto_quotes_options[coinstats_api_key]" value="%s" />',
            isset( $this->options['coinstats_api_key'] ) ? esc_attr( $this->options['coinstats_api_key']) : ''
        );
    }
}
