<?php

const CURRENCY_CONVERT = 'USD';

/**
 * getting all the data from API
 */
function ccq_get_coinmarketcap_api_data() {
	$options = get_option( Crypto_Currency_Quotes_Admin::OPTIONS );

	if ( ! $options ) {
		return false;
	}

	$api_url  = $options[ Crypto_Currency_Quotes_Admin::API_URL_FIELD_NAME ];
	$api_key  = $options[ Crypto_Currency_Quotes_Admin::API_KEY_FIELD_NAME ];
	$currency = $options[ Crypto_Currency_Quotes_Admin::CURRENCY_CONVERT_FIELD_NAME ];

	$request = wp_remote_get( $api_url . "listings/latest?start=1&limit=100&convert=" . $currency, [
		'headers' => [
			'X-CMC_PRO_API_KEY' => $api_key,
		]
	] );

	if ( is_wp_error( $request ) ) {
		return false;
	}

	$body = wp_remote_retrieve_body( $request );

	$response = json_decode( $body );

	if ( ! $response->data ) {
		return false;
	}

	$api_data = [];

	if ( $response->data ) {
		$api_data = transform_data( $response->data, $currency );
	}

	global $wpdb;
	$table_name = $wpdb->prefix . CCQ_PODS_DATA_TABLE;
	$results    = $wpdb->get_results( "SELECT * FROM $table_name" );

	$data = json_decode( json_encode( $results ), true );

	foreach ( $api_data as $item ) {

		$id = get_item_id_by( $data, $item['slug'], 'slug' );

		// If item id exists - update, else create new one

		$name               = $item['name'];
		$short_name         = $item['short_name'];
		$slug               = $item['slug'];
		$rank               = $item['rank'];
		$price              = $item['price'];
		$percent_change_1h  = ! empty( $item['percent_change_1h'] ) ? $item['percent_change_1h'] : 0;
		$percent_change_24h = ! empty( $item['percent_change_24h'] ) ? $item['percent_change_24h'] : 0;
		$percent_change_7d  = ! empty( $item['percent_change_24h'] ) ? $item['percent_change_7d'] : 0;
		$last_updated       = $item['last_updated'];
		$ratio              = $item['ratio'];

		if ( ! empty( $id ) ) {
			$wpdb->update(
				$table_name,
				array(
					'name'               => $name,
					'short_name'         => $short_name,
					'slug'               => $slug,
					'rank'               => $rank,
					'price'              => $price,
					'percent_change_1h'  => $percent_change_1h,
					'percent_change_24h' => $percent_change_24h,
					'percent_change_7d'  => $percent_change_7d,
					'last_updated'       => $last_updated,
					'ratio'              => $ratio
				),
				array( 'id' => $id )
			);
		} else {
			$wpdb->insert(
				$table_name,
				array(
					'id'                 => $id,
					'name'               => $name,
					'short_name'         => $short_name,
					'slug'               => $slug,
					'rank'               => $rank,
					'price'              => $price,
					'percent_change_1h'  => $percent_change_1h,
					'percent_change_24h' => $percent_change_24h,
					'percent_change_7d'  => $percent_change_7d,
					'last_updated'       => $last_updated,
					'ratio'              => $ratio
				)
			);
		}
	}
}

/**
 * Transforms data into PODS array
 *
 * @param $data
 * @param string $currency
 *
 * @return array
 */
function transform_data( $data, $currency = CURRENCY_CONVERT ) {
	$array = [];

	foreach ( $data as $item ) {
		$calc_ratio = number_format( 1 / $item->quote->{CCQ_CURRENCY_CONVERT}->price, 10, '.', '' );

		$array[] = [
			'name'               => $item->name,
			'short_name'         => $item->symbol,
			'slug'               => $item->slug,
			'rank'               => $item->cmc_rank,
			'price'              => number_format( $item->quote->{$currency}->price, 10, '.', '' ),
			'percent_change_1h'  => $item->quote->{$currency}->percent_change_1h,
			'percent_change_24h' => $item->quote->{$currency}->percent_change_24h,
			'percent_change_7d'  => $item->quote->{$currency}->percent_change_7d,
			'last_updated'       => $item->last_updated,
			'ratio'              => $calc_ratio,
		];
	}

	// Added additional mBTC based on BTC
	$additional_based_on = array_values( array_filter( $array, function ( $item ) {
		return $item['short_name'] == 'BTC';
	} ) );

	if ( count( $additional_based_on ) == 1 ) {
		$calc_ratio = number_format( ( $additional_based_on[0]['ratio'] * 1000 ), 6, '.', '' );

		$array[] = [
			'name'         => 'milli Bitcoin',
			'short_name'   => 'mBTC',
			'slug'         => 'mbtc',
			'rank'         => 1,
			'price'        => null,
			'last_updated' => $additional_based_on[0]['last_updated'],
			'ratio'        => $calc_ratio,
		];
	}

	return $array;
}


/**
 * Gets data from pods database
 *
 * @param int $limit
 *
 * @return array|bool
 */

function get_data( $limit = 10 ) {

	global $wpdb;
	$sql = "SELECT * FROM $wpdb->prefix" . CCQ_PODS_DATA_TABLE . "
	WHERE price > 0.000000 AND 
	rank != 99999 
	ORDER BY rank ASC 
	LIMIT $limit
	";

	$_crypto_currencies = $wpdb->get_results( $sql );

	$cryptos = array();
	if ( ! empty( $_crypto_currencies ) ) {
		foreach ( $_crypto_currencies as $crypto_currencies ) {
			$name               = $crypto_currencies->name;
			$short_name         = $crypto_currencies->short_name;
			$price              = $crypto_currencies->price;
			$percent_change_24h = ! empty( $crypto_currencies->percent_change_24h ) ? $crypto_currencies->percent_change_24h : '';

			$crypto_item                             = new stdClass;
			$crypto_item->name                       = $name;
			$crypto_item->short_name                 = $short_name;
			$crypto_item->price                      = $price;
			$crypto_item->percent_change_24h         = $percent_change_24h;
			$crypto_item->link = ! empty( $crypto_currencies->ref_link ) ? get_the_permalink($crypto_currencies->ref_link) : '/';

			$cryptos[] = $crypto_item;
		}
	}

	return $cryptos;
}

/**
 * Get currency symbol (add more in the future)
 *
 * @param $currency_short_name
 *
 * @return string
 */
function get_currency_symbol( $currency_short_name ) {
	switch ( $currency_short_name ) {
		case 'USD':
			$symbol = '$';
			break;
		case 'EUR':
			$symbol = '€';
			break;
		default:
			$symbol = '$';
			break;
	}

	return $symbol;
}

/**
 * Get item id by specific column
 *
 * @param $array
 * @param $value
 * @param string $search_by_column
 *
 * @return array
 */
function get_item_id_by( $array, $value, $search_by_column = 'slug' ) {
	if ( ! is_array( $array ) ) {
		return null;
	}

	$item = array_filter( $array, function ( $item ) use ( $value, $search_by_column ) {
		return $item[ $search_by_column ] === $value;
	} );

	// Reset keys
	if ( $item ) {
		return array_values( $item )[0]['id'];
	}
}

/**
 * Checks if array keys exists
 *
 * @param array $keys
 * @param array $arr
 *
 * @return bool
 */
function ccq_array_keys_exists( array $keys, array $arr ) {
	return ! array_diff_key( array_flip( $keys ), $arr );
}

/**
 * Add to menu edit cryptolist function
 *
 */

function cryptocurrencies_settings_menu() {
	add_options_page( 'Cryptocurrencies Settings', 'Cryptocurrencies', 'manage_options', 'cryptocurrencies-settings', 'cryptocurrencies_settings_page' );
}

// Render the Cryptocurrencies Settings page
function cryptocurrencies_settings_page() {
	// Check user capabilities
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	global $wpdb;
	$table_name = $wpdb->prefix . 'cryptocurrency';

	// Check for form submission and process updates
	if ( isset( $_POST['submit'] ) ) {
		// Process form submission
		$item_id            = intval( $_POST['item_id'] );
		$name               = sanitize_text_field( $_POST['name'] );
		$short_name         = sanitize_text_field( $_POST['short_name'] );
		$slug               = sanitize_title( $_POST['slug'] );
		$rank               = intval( $_POST['rank'] );
		$price              = floatval( $_POST['price'] );
		$percent_change_1h  = floatval( $_POST['percent_change_1h'] );
		$percent_change_24h = floatval( $_POST['percent_change_24h'] );
		$percent_change_7d  = floatval( $_POST['percent_change_7d'] );
		$last_updated       = sanitize_text_field( $_POST['last_updated'] );
		$ratio              = floatval( $_POST['ratio'] );
		$ref_link           = esc_url_raw( $_POST['ref_link'] );


		$update_result = $wpdb->update(
			$table_name,
			array(
				'name'               => $name,
				'short_name'         => $short_name,
				'slug'               => $slug,
				'rank'               => $rank,
				'price'              => $price,
				'percent_change_1h'  => $percent_change_1h,
				'percent_change_24h' => $percent_change_24h,
				'percent_change_7d'  => $percent_change_7d,
				'last_updated'       => $last_updated,
				'ratio'              => $ratio,
				'ref_link'           => $ref_link
			),
			array(
				'id' => $item_id
			)
		);

		if ( $update_result !== false ) {
			echo '<div class="notice notice-success"><p>Item updated successfully.</p></div>';
		} else {
			echo '<div class="notice notice-error"><p>Error updating item.</p></div>';
		}
	}

	// Render the form for editing items


	global $wpdb;
	$table_name = $wpdb->prefix . 'cryptocurrency';

	$item_id = isset( $_GET['item_id'] ) ? intval( $_GET['item_id'] ) : 0;

	if ( ! empty( $item_id ) ) {

		$item = $wpdb->get_row( "SELECT * FROM $table_name WHERE id = $item_id" );

		$posts = get_pages();

		$pages = [];

		foreach ( $posts as $post ) {    // Pluck the id and title attributes
			$pages[] = array( 'id' => $post->ID, 'slug' => $post->post_name, 'title' => $post->post_title );
		}

		if ( ! $item ) {
			echo '<div class="notice notice-error"><p>Invalid item ID.</p></div>';

			return;
		}
		?>
        <div class="wrap">
            <h1>Cryptocurrencies Settings</h1>

            <form method="post" action="">
                <input type="hidden" name="item_id" value="<?php echo $item->id; ?>">
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row"><label for="name">Name</label></th>
                        <td><input type="text" name="name" id="name" value="<?php echo esc_attr( $item->name ); ?>"
                                   class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="short_name">Short Name</label></th>
                        <td><input type="text" name="short_name" id="short_name"
                                   value="<?php echo esc_attr( $item->short_name ); ?>" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="slug">Slug</label></th>
                        <td><input type="text" name="slug" id="slug" value="<?php echo esc_attr( $item->slug ); ?>"
                                   class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="rank">Rank</label></th>
                        <td><input type="number" name="rank" id="rank" value="<?php echo esc_attr( $item->rank ); ?>"
                                   class="regular-text"></td>
                    </tr>
                    <tr class="row">
                        <th scope="row"><label for="ref_link_name">Reference Name</label></th>
                        <td style="position: relative;">
                            <input type="text" name="ref_link_name" id="crypto_page_title" class="regular-text"
                                   value="<?= get_the_title($item->ref_link); ?>"><br/>
                            <input type="text" name="ref_link" id="ref_link" class="regular-text hidden" 
                                   value="<?= esc_attr( $item->ref_link ); ?>">
                            <div id="ref_link_autocomplete" class="autocomplete-items"></div>
                            <script>
                                // Get a list of WordPress pages for the autocomplete input.
                                var pages = <?php echo wp_json_encode( $pages ); ?>;

                                // Initialize the autocomplete function.
                                var inputRefLink = document.getElementById("ref_link");
                                var pageTitle = document.getElementById("crypto_page_title");
                                var divAutocomplete = document.getElementById("ref_link_autocomplete");

                                pageTitle.addEventListener("input", function (e) {
                                    var term = e.target.value.toLowerCase();

                                    var matches = [];
                                    pages.forEach(function (page) {
                                        if (page.title.toLowerCase().indexOf(term) >= 0) {
                                            matches.push(page);
                                        }
                                    });
                                    showAutocomplete(matches);
                                });

                                function showAutocomplete(matches) {
                                    var html = "";
                                    if (matches.length === 0) {
                                        divAutocomplete.innerHTML = "";
                                        return;
                                    }
                                    matches.forEach(function (match) {
                                        html += "<div>" + match.title + "</div>";
                                    });
                                    divAutocomplete.innerHTML = html;
                                    divAutocomplete.style.display = "block";

                                    var autocompleteItems = divAutocomplete.getElementsByTagName("div");
                                    for (var i = 0; i < autocompleteItems.length; i++) {
                                        autocompleteItems[i].addEventListener("click", function (e) {
                                            inputRefLink.value = getSlugByTitle(e.target.innerText);
											pageTitle.value = e.target.innerText;
                                            divAutocomplete.style.display = "none";
                                        });
                                    }
                                }

                                function getSlugByTitle(title) {
                                    for (var i = 0; i < pages.length; i++) {
                                        if (pages[i].title === title) {
                                            return pages[i].id;
                                        }
                                    }
                                    return "";
                                }
                            </script>
                            <style>
                                .autocomplete-items {
                                    position: absolute;
                                    top: 50px;
                                    left: 10px;
                                    padding: 10px;
                                    box-shadow: 0 0 0 transparent;
                                    border-radius: 4px;
                                    border: 1px solid #8c8f94;
                                    background-color: #fff;
                                    color: #2c3338;
                                    display: none;
                                    z-index: 999;
                                }

                                .autocomplete-items > div {
                                    padding: 3px;
                                    border-bottom: 1px solid #f1f1f1;
                                }

                                .autocomplete-items > div:hover {
                                    background: #f1f1f1;
                                }
                            </style>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" name="id" value="<?php echo $item->id; ?>">

				<?php submit_button( 'Save Changes', 'primary', 'submit_cryptocurrency' ); ?>
            </form>

        </div>
		<?php
	}
    else {
	    echo '<table class="wp-list-table widefat fixed striped table-view-list pages">';
	    // Retrieve all items from the wp_cryptocurrency table
	    $items = $wpdb->get_results( "SELECT * FROM $table_name" );

// Loop through each item and display it in a table row
	    foreach ( $items as $item ) {
            $checked = $item->status > 0 ? 'checked' : '';
		    echo "<tr>";
		    echo "<td>" . esc_html( $item->id ) . "</td>";
		    echo "<td>" . esc_html( $item->name ) . "</td>";
		    echo "<td>" . esc_html( $item->short_name ) . "</td>";
		    echo "<td>" . esc_html( $item->slug ) . "</td>";
		    echo "<td>" . esc_html( $item->rank ) . "</td>";
		    echo "<td>" . esc_html( $item->ratio ) . "</td>";
		    echo "<td><input type='checkbox' id='item$item->id' class='status-toggle' data-id='$item->id' $checked> </td>";
            echo "<td>" . esc_html( get_the_title( $item->ref_link ) ) . "</td>";
		    echo "<td><a href=\"/wp/wp-admin/options-general.php?page=cryptocurrencies-settings&item_id=$item->id\">Edit</a></td>";
		    echo "</tr>";
	    }
	    echo '</table>';
        ?>
            <script>
                document.querySelectorAll('.status-toggle').forEach(item => {
                    item.addEventListener('change', function() {
                        var itemId = this.getAttribute('data-id');
                        var status = this.checked ? 1 : 0;
                        updateStatus(itemId, status);
                    });
                });

                function updateStatus(itemId, status) {
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', 'admin-ajax.php', true);
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                            console.log('Status updated to ' + status);
                        }
                    };

                    xhr.send('action=update_status&item_id=' + itemId + '&status=' + status);
                }
            </script>
        <?php
    }
}

// Save settings page form data
function save_cryptocurrency_settings() {
	global $wpdb;
	if ( isset( $_POST['submit_cryptocurrency'] ) ) {
		$id                 = $_POST['id'];
		$name               = $_POST['name'];
		$short_name         = $_POST['short_name'];
		$slug               = $_POST['slug'];
		$rank               = $_POST['rank'];
		$price              = $_POST['price'];
		$percent_change_1h  = $_POST['percent_change_1h'];
		$percent_change_24h = $_POST['percent_change_24h'];
		$percent_change_7d  = $_POST['percent_change_7d'];
		$last_updated       = $_POST['last_updated'];
		$ratio              = $_POST['ratio'];
		$ref_link           = $_POST['ref_link'];

		$data = array(
			'name'               => $name,
			'short_name'         => $short_name,
			'slug'               => $slug,
			'rank'               => $rank,
			'price'              => $price,
			'percent_change_1h'  => $percent_change_1h,
			'percent_change_24h' => $percent_change_24h,
			'percent_change_7d'  => $percent_change_7d,
			'last_updated'       => $last_updated,
			'ratio'              => $ratio,
			'ref_link'           => $ref_link
		);

		$where = array(
			'id' => $id
		);

		$wpdb->update( 'wp_cryptocurrency', $data, $where );
	}
}

add_action( 'admin_init', 'save_cryptocurrency_settings' );
?>