<?php

class Crypto_Currency_Quotes_Widget extends WP_Widget {

    const WIDGET_ID = 'crypto_currency_quotes_widget';
    const WIDGET_NAME = 'Crypto currency quotes widget';

    /**
     * Crypto_Currency_Quotes_Widget constructor.
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => self::WIDGET_ID,
            'description' => 'A widget for displaying top crypto currency quotes',
        );

        add_filter('widget_title', array($this, 'custom_widget_title'));

        parent::__construct(self::WIDGET_ID, self::WIDGET_NAME, $widget_ops);
    }

    /**
     * Frontend display of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance) {
        $data = get_data();
        $currency = get_currency_symbol(get_option(Crypto_Currency_Quotes_Admin::OPTIONS)[Crypto_Currency_Quotes_Admin::CURRENCY_CONVERT_FIELD_NAME]);
        $title = '';

        extract( $args );
        echo $before_widget;

        if ( ! empty( $instance['title'] ) ) {
            $title = $instance['title'];
        }

        $title = apply_filters('widget_title', $title);

        $template = ! empty( $instance['template'] ) ? $instance['template'] : 'display.php';

        $templates_dir = WP_PLUGIN_DIR . '/crypto-currency-quotes/templates/widget';
        $all_templates = scandir($templates_dir);
        $list_templates = array_diff($all_templates, array('.', '..', 'admin.php'));
        global $wp;
        $current_url = home_url($wp->request) . '/';

        include Crypto_Currency_Quotes_PATH . '/templates/widget/' . $template;

        echo $after_widget;
    }

    /**
     * Admin menu form logic
     *
     * @param array $instance
     * @return string|void
     */
    public function form($instance) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__('Title', 'text_domain');
        $template = ! empty( $instance['template'] ) ? $instance['template'] : 'display.php';

        $templates_dir = WP_PLUGIN_DIR . '/crypto-currency-quotes/templates/widget';
        $all_templates = scandir($templates_dir);
        $list_templates = array_diff($all_templates, array('.', '..', 'admin.php'));

        include Crypto_Currency_Quotes_PATH . 'templates/widget/admin.php';
    }

    /**
     *  Add custom br (break line) for widget title
     *
     * @param $title
     * @return mixed
     */
    function custom_widget_title($title) {
        $title = str_replace( 'line_break', '<br class="break-line"/>', $title );

        return $title;
    }

    /**
     * Admin logic to updated widget parameters
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance) {
        $instance = [];
        $instance['title'] = (! empty( $new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['template'] = (! empty( $new_instance['template'])) ? strip_tags($new_instance['template']) : '';

        return $instance;
    }
}


function crypto_quotes_sidebar() {
    register_sidebar(
        [
            'name' => __( 'Crypto quotes sidebar', 'landx' ),
            'id' => 'crypto_quotes_sidebar',
            'description' => __( 'Crypto quotes sidebar', 'landx' ),
            'before_widget' => '<div class="crypto-quotes-widget-wrapper"><div class="container">',
            'after_widget' => '</div></div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]
    );
}

add_action('widgets_init', 'crypto_quotes_sidebar');

add_action('widgets_init', function () {
    register_widget( 'Crypto_Currency_Quotes_Widget' );
});