# Crypto quotes plugin

Crypto quotes plugin that displays top cryptocurrencies from coinmarket API

## Installation

Activate plugin - you're good to go.

In order to change coinmarket API KEY, go to plugin settings and change it there.


## Development
In order to continue developing this plugin:

```bash
npm install
```
In order to run gulp and watch styles for changes (this will open new browser with browsersync which will reload only styles without reloading page):
```bash
npm run gulp watch:sass
```
