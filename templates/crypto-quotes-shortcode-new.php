<?php
/** @var $api_key */
/** @var $api_url */
/** @var $language */
/** @var $json_coin */


$text_top_crypto = get_option('crypto_top_crypto__guru_' . $language);
$text_rating = get_option('rating_title__guru');
?>

<div class="flex items-center justify-between gap-4">
    <div class="text-2xl font-medium"><?= $text_top_crypto ?></div>
    <div class="hidden lg:flex lg:gap-2">
        <div id="scroll-btn-prev-crypto"
             class="cursor-pointer flex items-center justify-center bg-white bg-opacity-40 hover:bg-neutral-200 border border-opacity-20 border-black rounded-2xl w-11 h-11 disabled">
            <span class="icon icon-24 icon-arrow_back"></span>
        </div>
        <div id="scroll-btn-next-crypto"
             class="cursor-pointer flex items-center justify-center bg-white bg-opacity-40 hover:bg-neutral-200 border border-opacity-20 border-black rounded-2xl w-11 h-11">
            <span class="icon icon-24 icon-arrow_forward"></span>
        </div>
    </div>
</div>
<div class="mt-5">
    <ul id="scroll-container-crypto"
        class="relative flex gap-3 sm:gap-6 snap-x snap-proximity overflow-x-auto lg:overflow-hidden lg:justify-between -mx-5 sm:mx-0 scroll-container !list-none [& a]:no-underline"></ul>
</div>

<script>
    const nextBtnCcq = document.getElementById("scroll-btn-next-crypto");
    const prevBtnCcq = document.getElementById("scroll-btn-prev-crypto");
    const containerCcq = document.getElementById("scroll-container-crypto");
    const containerRectCcq = containerCcq.getBoundingClientRect();
    nextBtnCcq.addEventListener("click", () => scrollHorizontally("next", containerCcq, containerRectCcq));
    prevBtnCcq.addEventListener("click", () => scrollHorizontally("previous", containerCcq, containerRectCcq));
    containerCcq.addEventListener("scroll", () => disableScrollButtons(containerCcq, containerRectCcq, prevBtnCcq, nextBtnCcq));
    const json_coin_ccq = '<?=$json_coin?>';
    const crypto_ccq = JSON.parse(json_coin_ccq);
    // Fetch options
    const options_ccq = {
        method: 'GET',
        headers: {
            accept: 'application/json',
            'X-API-KEY': '<?= $api_key ?>'
        }
    };
    document.addEventListener('DOMContentLoaded', async function () {

        (async () => {
            console.log('Loading crypto ...')
            const filteredCoins = await getCoinsCcq();
            // Clear existing list
            containerCcq.innerHTML = '';

            // Create and append list items
            filteredCoins.forEach(coin => {
                const listItem = createListItemCcq(coin);
                containerCcq.appendChild(listItem);
            });
            // Set interval to update prices every minute (60000 milliseconds)
            setInterval(fetchDataAndUpdateUICcq, 5000);
        })();
    })

    async function getCoinsCcq() {
        try {
            const response = await fetch('<?=$api_url?>coins', options_ccq);
            const data = await response.json();
            filteredCoins = data.result
                .filter(item => crypto_ccq.some(cryptoItem => cryptoItem.symbol.toLowerCase() === item.symbol.toLowerCase()))
                .map(item => {
                    // Find the matching cryptoItem to get the link
                    const cryptoItem = crypto_ccq.find(cryptoItem => cryptoItem.symbol.toLowerCase() === item.symbol.toLowerCase());
                    // Add the link field to the item
                    return {
                        ...item,
                        link: cryptoItem ? cryptoItem.link : null // Add link from cryptoItem, or null if not found
                    };
                });
            //console.log(filteredCoins);
            return filteredCoins; // Return or process filteredCoins here
        } catch (err) {
            console.error(err);
        }
    }

    // Function to create list item
    function createListItemCcq(coin) {
        // Destructure the required properties
        const {icon, id, price, priceChange1d, name, link} = coin;

        const priceChangeClass = priceChange1d < 0 ? 'text-red-400' : 'text-teal-400';
        const priceIconClass = priceChange1d > 0 ? 'rotate-180' : '';
        const priceId = `price-${id}`;
        const priceChangeId = `price-change-${id}`;

        // Create list item element
        const listItem = document.createElement('li');
        listItem.className = 'scroll-item snap-start shrink-0 first:pl-5 last:pr-5 first:sm:pl-0 last:sm:pr-0';

        // Set inner HTML
        listItem.innerHTML = `
                        <a href="${link}" class="inline-flex p-3 w-full sm:w-[292px] bg-white border border-white hover:border-gray-200 active:border-gray-700 rounded-2xl overflow-hidden !no-underline" target="_blank">
                            <img loading="lazy" class="shrink-0 w-12 h-12 rounded-full overflow-hidden !mb-0" src="${icon}" alt="${id}">
                            <div class="pl-3 flex-grow flex flex-col justify-between">
                                <div class="flex justify-between text-base gap-5">
                                    <span class="font-semibold">${name}</span>
                                    <span class="whitespace-nowrap" id="${priceId}">$ ${priceFormat(price.toFixed(2))}</span>
                                </div>
                                <div class="flex text-sm gap-5 justify-between">
                                    <span>${name} <?=$text_rating?> </span>
                                    <span class="${priceChangeClass} whitespace-nowrap" id="${priceChangeId}"><span class="${priceIconClass} inline-block">&#9660</span> ${priceChange1d} %</span>
                                </div>
                            </div>
                        </a>
                    `;

        return listItem;
    }


    async function fetchDataAndUpdateUICcq() {

        const filteredCoins = await getCoinsCcq();
        filteredCoins.forEach(coin => {
            // Update price and price change if the element exists
            const priceElement = document.getElementById(`price-${coin.id}`);
            const priceChangeElement = document.getElementById(`price-change-${coin.id}`);

            if (priceElement) {
                priceElement.textContent = '$ ' + priceFormat(coin.price.toFixed(2));
            }

            if (priceChangeElement) {
                const priceChangeClass = coin.priceChange1d < 0 ? 'text-red-400' : 'text-teal-400';
                const priceIconClass = coin.priceChange1d > 0 ? 'rotate-180' : '';
                const priceIcon = coin.priceChange1d < 0 ? '&#9660;' : '&#9650;'; // Down and up arrow icons

                priceChangeElement.className = `whitespace-nowrap ${priceChangeClass}`;
                priceChangeElement.innerHTML = `<span class="${priceIconClass} inline-block">${priceIcon}</span> ${coin.priceChange1d} %`;
            }
        });
    }

</script>

<style>
    .content #scroll-container-crypto a > img, .content #scroll-container-crypto p > img {
        margin-bottom: 0 !important;
    }
</style>