<div class="crypto-quotes">
    <?php if (! empty($atts['title'])) { ?>
        <div class="top-quotes">
            <?= $atts['title'] ?>
        </div>
    <?php } ?>
    <ul class="crypto-quotes-slides">
        <?php foreach ($data as $item) { ?>
            <?php
                if ($item->percent_change_24h > 0)
                    { $textColour = 'green'; }
                elseif ($item->percent_change_24h === 0)
                    { $textColour = 'white'; }
                else
                    { $textColour = 'red'; }
            ?>

            <li class="<?= $textColour; ?>">
                <div class="wrap">
                    <div class="wrap-list">
                        <img class="crypto-icon"
                             src="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($item->short_name) . '.svg' ?>"
                             data-toggle="tooltip" title="<?= $item->name ?>" data-placement="top"
                        />
                        <span class="short-name"><?= $item->short_name ?></span>
                    </div>
                    <div class="wrap">
                        <span class="price"><?= $currency . number_format((float)$item->price, 2, '.', ',') ?></span>
                        <span class="change">
                            <?php if (($item->percent_change_24h > 0)) echo '+'; ?><?= $item->percent_change_24h ?>%
                        </span>
                    </div>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>