<div class="crypto-quotes-widget">
    <?php if (! empty($title)) { ?>
        <div class="top-quotes">
            <?= $title ?>
        </div>
    <?php } ?>
    <ul class="crypto-quotes-slides">
        <?php foreach ($data as $item) { ?>
            <?php
                if ($item->percent_change_24h > 0)
                    { $textColour = 'green'; }
                elseif ($item->percent_change_24h === 0)
                    { $textColour = 'white'; }
                else
                    { $textColour = 'red'; }

                if (empty($item->crypto_bookmakers_page_url)) {
                    continue;
                }
            ?>

            <li class="<?= $textColour; ?>">
                <div class="wrap-list">
                    <div class="wrap-inline">
                        <div class="crypto-quote-inline">
                            <img class="crypto-icon inline"
                                 src="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($item->short_name) . '.svg' ?>"
                                 data-toggle="tooltip" title="<?= $item->name ?>" data-placement="top"
                            />
                            <span class="short-name"><?= $item->short_name ?></span>
                            <span class="price"><?= $currency . number_format((float)$item->price, 2, '.', ',') ?></span>
                            <span class="change"><?php if (($item->percent_change_24h > 0)) echo '+'; ?><?= $item->percent_change_24h ?>%</span>
                        </div>
                        <?php if (!empty($item->crypto_bookmakers_page_url)): ?>
                            <div class="crypto-quote-filter-link">
                                <a href="<?= $item->crypto_bookmakers_page_url ?>"<?php if ($current_url == $item->crypto_bookmakers_page_url): ?> class="current"<?php endif; ?>><?= $item->name ?> <?= get_option('bookmaker__guru') ? get_option('bookmaker__guru') : 'bookmakers'; ?></a>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
            </li>
        <?php } ?>
    </ul>
</div>