<div class="crypto-quotes-widget">
    <?php if (! empty($title)) { ?>
        <div class="top-quotes">
            <?= $title ?>
        </div>
    <?php } ?>
    <ul class="crypto-quotes-slides">
        <?php foreach ($data as $item) { ?>
            <?php
                if ($item->percent_change_24h > 0)
                    { $textColour = 'green'; }
                elseif ($item->percent_change_24h === 0)
                    { $textColour = 'white'; }
                else
                    { $textColour = 'red'; }
            ?>

            <li class="<?= $textColour; ?>">
                <div class="wrap-list">
                    <div class="wrap">
                        <div class="wrap-column">
                            <span class="short-name"><?= $item->short_name ?></span>
                            <img class="crypto-icon"
                                 src="<?= Crypto_Currency_Quotes_URL . 'assets/images/cryptos/' . strtolower($item->short_name) . '.svg' ?>"
                                 data-toggle="tooltip" title="<?= $item->name ?>" data-placement="top"
                            />
                        </div>
                        <div class="wrap-column price-column">
                            <span class="price"><?= $currency . number_format((float)$item->price, 2, '.', ',') ?></span>
                            <span class="change">
                                <?php if (($item->percent_change_24h > 0)) echo '+'; ?><?= $item->percent_change_24h ?>%
                            </span>
                        </div>
                    </div>

                </div>
            </li>
        <?php } ?>
    </ul>
</div>